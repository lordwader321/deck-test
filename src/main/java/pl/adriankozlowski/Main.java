package pl.adriankozlowski;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Main {
	
	public static final int PLAYERS = 3;
	public static final int CARDS_FOR_PLAYERS = 13;
	
    public static void main(String[] args) {    			
        Main main = new Main();
        Collection<Card> cards = main.generateDeck();
        cards = main.shuffle(cards);
        Map<String, List<Card>> distributedCards = main.dealCards(cards);
    }

    public Collection<Card> shuffle(Collection<Card> cards) {
    	
    	List<Card> newList = new ArrayList<>(cards);
    	Collections.shuffle(newList);
    	
    	if(!newList.isEmpty()) {
    		return newList;
    	}
    	
    	
        return null;
    }

    public Map<String, List<Card>> dealCards(Collection<Card> cards) {
    	
    	List<Card> t1List = new ArrayList<>();
    	List<Card> t2List = new ArrayList<>();
    	List<Card> t3List = new ArrayList<>();
    	Map<String, List<Card>> dCards = new HashMap<String, List<Card>>();
    	
        int cardsPerPlayer = cards.size() / PLAYERS;
        
    	int count = 1;
    	for (Card c : cards) {
			
    		switch ( (count%PLAYERS) ) {
				case 1: {
					t1List.add(c);
					break;
				}
				case 2:{
					t2List.add(c);
					break;
	    		}
				case 0:{
					t3List.add(c);
					break;
				}
    		}
			
    		if(t3List.size() == cardsPerPlayer || t3List.size() == CARDS_FOR_PLAYERS) {
				break;
			}
    		
			count++;
			
    	}
    	
    	dCards.put("tile1", t1List);
    	dCards.put("tile2", t2List);
    	dCards.put("tile3", t2List);
    	
    	cards.removeAll(t1List);
    	cards.removeAll(t2List);
    	cards.removeAll(t3List);
    	
    	
    	if(!dCards.isEmpty()) {
    		return dCards;
    	}
    	
        return null;
    }

    public Collection<Card> generateDeck() {
    	
    	List<Card> deck = new ArrayList<Card>();
    	deck.clear();
    	
    	for (Card.Value v : Card.Value.values()) {
    		for (Card.Suit s : Card.Suit.values()) {
    			deck.add(new Card(v, s));
    		}
			
		}
    	
    	if(!deck.isEmpty()) {
    		return deck;
    	}
    	
        return null;
    }
}
