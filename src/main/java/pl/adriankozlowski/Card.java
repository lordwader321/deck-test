package pl.adriankozlowski;

public class Card {
    private Value value;
    private Suit suit;
    
    public Card() {
		super();
	}

	public Card(Value value, Suit suit) {
		super();
		this.value = value;
		this.suit = suit;
	}

	public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public enum Value {
        TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
    }

    public enum Suit {
        HEARTS, CLUBS, SPADES, DIAMONDS
    }
}
